pipeline {
    agent {
        label 'linux'
    }

    triggers {
        cron('0 0 * * *')
    }
    stages {
        stage('Clone source code') {
            steps {
                try {
                    echo "Cloning the source code"
                    sh "git clone git@bitbucket.org:SivakumarBalu/jenkin-hound.git"
                } catch(Err) {
                    throw Err
                }
               
            }
        }
        stage('Test') {
            steps {
                try {
                    sh '''#!/bin/bash -l
                    export HOUNDIFY_PATH=$WORKSPACE/houndify_python_3_sdk/
                    export PYTHONPATH=$HOUNDIFY_PATH
                    cd $HOUNDIFY_PATH
                    PYTHON_3 = $(which python3)
                    $PYTHON_3 -m unittest tests/test_houndify.py -v
                    '''
                } catch(Err) {
                    throw Err
                }
                }
            }
    }
}